package org.bitbucket.ap3amac.codesysudpvarlist;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class Main {

	public static void main(String[] args) throws Exception {

		InetAddress inetAddress;
		byte[] inetAddressIp = {(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF};
		int port;
		int delayTimeMs;
		int dataLength = 33;
		byte[] data = new byte[dataLength];

		switch(args.length) {
			case 1:
				port = Integer.parseInt(args[0]);
				delayTimeMs = 2000;
				break;
			case 2:
				port = Integer.parseInt(args[0]);
				delayTimeMs = Integer.parseInt(args[1]);
				break;
			default:
				port = 1203;
				delayTimeMs = 2000;
		}

		inetAddress = InetAddress.getByAddress(inetAddressIp);

		// Identity 0-3
		data[1] = (byte) '-';
		data[2] = (byte) 'S';
		data[3] = (byte) '3';

		//Index 8-9
		data[8] = (byte) 1;

		//Items 12-13
		data[12] = (byte) 7;

		//Length 14-15
		data[14] = (byte) dataLength;

		//Flags 18
		data[18] = (byte) 0x03;

		//Checksum 19
		data[19] = (byte) 0xF8;

		//Data 20-32
		data[20] = (byte) 1;

		data[22] = (byte) 1;

		data[24] = (byte) 0xAA;
		data[25] = (byte) 0xD0;
		data[26] = (byte) 0x0F;
		data[27] = (byte) 0x49;
		data[28] = (byte) 0x40;
		data[29] = (byte) 0x50;
		data[30] = (byte) 0x69;

		DatagramPacket packet = new DatagramPacket(data, data.length, inetAddress, port);
		DatagramSocket toSocket = new DatagramSocket();

		System.out.println("\nSimpleUdpTransmitter.jar <PORT> <DELAY_TIME_MS>\n\ndefault PORT= 1203\ndefault DELAY_TIME_MS= " +
				"2000ms\n");

		System.out.println("---Settings---");
		System.out.println("Destination address: " + inetAddress + ":" + port);
		System.out.println("Delay time: " + delayTimeMs + "ms");

		System.out.println("\n---Frame---");
		int i = 0;
		System.out.print("Identity:\t");
		for(; i < 4; i++)
			System.out.printf(String.format("%02X ", data[i]));

		System.out.print("\nID:\t\t\t");
		for(; i < 8; i++)
			System.out.printf(String.format("%02X ", data[i]));

		System.out.print("\nIndex:\t\t");
		for(; i < 10; i++)
			System.out.printf(String.format("%02X ", data[i]));

		System.out.print("\nSubidex:\t");
		for(; i < 12; i++)
			System.out.printf(String.format("%02X ", data[i]));

		System.out.print("\nItems:\t\t");
		for(; i < 14; i++)
			System.out.printf(String.format("%02X ", data[i]));

		System.out.print("\nLength:\t\t");
		for(; i < 16; i++)
			System.out.printf(String.format("%02X ", data[i]));

		System.out.print("\nCounter:\t");
		for(; i < 18; i++)
			System.out.printf(String.format("%02X ", data[i]));

		System.out.print("\nFlags:\t\t" + String.format("%02X ", data[i++]));

		System.out.print("\nChecksum:\t" + String.format("%02X ", data[i++]));

		System.out.println("\n\n---Data---");

		for(; i < 24; i++)
			System.out.print("BOOL:\t" + String.format("%02X ", data[i]) + "\n");

		System.out.print("BYTE:\t" + String.format("%02X ", data[i++]));

		System.out.print("\nREAL:\t");
		for(; i < 29; i++)
			System.out.printf(String.format("%02X ", data[i]));

		System.out.print("\nSTR(3):\t");
		for(; i < 32; i++)
			System.out.printf(String.format("%02X ", data[i]));

		System.out.println("\n\n---Start transmission---");

		int counter = 0;
		while(!Thread.interrupted()) {

			//Counter 16-17
			counter++;
			data[16] = (byte) counter;
			data[17] = (byte) (counter >> 8);

			packet.setData(data);

			toSocket.send(packet);

			System.out.println("Fraim No." + counter);
			Thread.sleep(delayTimeMs);
		}
	}
}
